#include <stdio.h>

/*** T I F F ***/

typedef struct {
  unsigned int width;
  unsigned int height;
  unsigned long rowsPerStrip;
  unsigned long stripCount;
  unsigned long stripOffsets;
} TifImageMeta_t;

#define MAX_WIDTH 328

TifImageMeta_t tifLoadMeta(FILE *f);
int tifLoadEGA(FILE *f, TifImageMeta_t meta, unsigned int vidOffset, int maxY, unsigned int w);
int tifLoad(FILE *f, TifImageMeta_t meta, unsigned int far *planeBuf, int maxY, int yRepeat, int planes);
