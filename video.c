#include <dos.h>
#include "video.h"

void vid_cleanup() {
  setTextMode();
}

void setSplitScreen(unsigned int y) {
  int val;
  outport(REG_CRTC, 0x18 | (y << 8));
  outp(REG_CRTC, 7);
  val = inp(REG_CRTC + 1);
  val &= ~0x10;
  val |= (y & 0x100) >> 4;
  outp(REG_CRTC + 1, val);
  outp(REG_CRTC, 9);
  val = inp(REG_CRTC + 1);
  val &= ~0x40;
  outp(REG_CRTC + 1, val);
}

void unsetSplitScreen() {
  outport(REG_CRTC, 0xff18);
  outport(REG_CRTC, 0x1107);
  outport(REG_CRTC, 0x0f09);
}


void setDisplayOffset(unsigned int offset) {
  outport(REG_CRTC, 0x0c | (offset & 0xff00));
  outport(REG_CRTC, 0x0d | (offset << 8));
}

void setHorizontalPan(int offset) {
  inp(0x3da); // INPUT_STATUS_1?
  outp(REG_AC, 0x13 | 0x20);
  outp(REG_AC, offset);
}

