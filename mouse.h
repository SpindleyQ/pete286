/*** M O U S E ***/
typedef struct {
  unsigned int x;
  unsigned int y;
  unsigned int buttons;
} Mouse_t;

extern Mouse_t MOUSE;

void mouse_init();

#define mouse_hide() asm { mov ax, 02h; int 33h }
#define mouse_show() asm { mov ax, 01h; int 33h }
