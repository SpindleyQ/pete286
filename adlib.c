#include "adlib.h"

static void adlib_wait(int delay) {
  int i;
  for (i = 0; i < delay; i ++) adlib_read();
}

void adlib_write(int reg, int val) {
  int i;
  outp(0x388, reg);
  adlib_wait(6);
  outp(0x389, val);
  adlib_wait(35);
}

void adlib_reset() {
  int i;
  for (i = 0; i < 0xff; i ++) {
    adlib_write(i, 0);
  }
}

void adlib_init() {
  adlib_reset();
  atexit(adlib_reset);
}
