/*** T I L E S ***/
#ifndef __TILES_H__
#define __TILES_H__

void tile_init();

void loadTiles(unsigned int tilesOffset, unsigned int far *memTiles);
void loadMap(unsigned char *map, unsigned int w, unsigned int h);
void drawSprite(unsigned int far *sprite, int x, int y, char *remap);
void scroll(int newX, int newY);
void drawScreen();

void blit32x32(unsigned int offsetFrom, unsigned int offsetTo);
void blitMemToVid(unsigned int offset, unsigned int far *mem, unsigned int planeStride, int count);

void writeTile(unsigned int *buf, unsigned int far *tile);
void overlaySprite(unsigned int *buf, unsigned int far *sprite, int shift, int yStart, char *remap);
void paintBuffer(unsigned int *buf, unsigned int vidOffset);

#define PAGE_TILES_W 21
#define PAGE_TILES_H 14
#define PAGE_TILES_COUNT (PAGE_TILES_H * PAGE_TILES_W)
#define PAGE_STRIDE (PAGE_TILES_W << 1)

#define NUM_BUFFERS 20
#define BUF_WSTRIDE 16
#define BUF_WSIZE (BUF_WSTRIDE * 4)

typedef struct {
  unsigned int w;
  unsigned int h;
  int scrollX;
  int scrollY;
  unsigned int pageOffset[2];
  unsigned char dirty[2][PAGE_TILES_COUNT];
  unsigned int tilesOffset;
  unsigned int far *memTiles;
  unsigned char *map;
  unsigned int buffer[NUM_BUFFERS][BUF_WSIZE];
  unsigned int bufferOffset[NUM_BUFFERS];
  unsigned char currentPage;
  unsigned char nextBuffer;
  unsigned char firstBuffer;
} TiledScreen_t;

extern TiledScreen_t screen;

#endif