#define TIMER_60HZ 0x4dae
#define TIMER_50HZ 0x5d37
#define TIMER_40HZ 0x7486
#define TIMER_30HZ 0x965c
#define TIMER_20HZ 0xe90b
#define TIMER_18HZ 0xffff

extern volatile unsigned int timer_counter;

void timer_init(unsigned int rate);
void timer_setrate(unsigned int rate);
void timer_setcallback(void (*cb)());
