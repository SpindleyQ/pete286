#define SER_COM1 0
#define SER_COM2 1
#define SER_COM3 2
#define SER_COM4 3

#define SER_8N1 0x03

#define BAUD_50     0x0900
#define BAUD_110    0x0417
#define BAUD_220    0x020c
#define BAUD_300    0x0180
#define BAUD_600    0x00c0
#define BAUD_1200   0x0060
#define BAUD_2400   0x0030
#define BAUD_4800   0x0018
#define BAUD_9600   0x000c
#define BAUD_19200  0x0006
#define BAUD_38400  0x0003
#define BAUD_57600  0x0002
#define BAUD_115200 0x0001

#define SER_READ_BUFFER_SIZE 64
#define SER_NODATA -1

void ser_init(int port, int baudrate, int protocol);
int ser_poll();
void ser_write_byte(char byte);
void ser_write(char *str);
int ser_getline(char *line);
